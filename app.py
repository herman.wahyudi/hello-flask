from flask import Flask
from flask import request
import socket, requests, os

app = Flask(__name__)

@app.route('/')
def index():
    app.logger.info(os.environ.get("NAME"))
    return 'Deploy version 1.1!'

@app.route('/health')
def health():
    return 'Health check 200!'

@app.route('/tcp-connect', methods=['GET'])
def tcpConnect():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
      ip = request.args.get('ip')
      port = request.args.get('port')

      s.connect((ip, int(port)))
      s.shutdown(2)
      return "Connected to {}:{}".format(ip, port)
    except:
      return "Failed to connect {}:{}!".format(ip, port)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
