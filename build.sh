#!/bin/sh

docker build -t hello-flask:$1 .

docker tag hello-flask:$1 hermanwahyudi/hello-flask:$1

docker push hermanwahyudi/hello-flask:$1
